public with sharing class Day01 {
  public interface IDay01File {
    StaticResource getDay01File();
  }
  private IDay01File day01File;

  public Day01() {
    this.day01File = new Day01File();
  }

  public Day01(IDay01File day01File) {
    this.day01File = day01File;
  }

  public Integer part1() {
    StaticResource day1InputFile = this.day01File.getDay01File();
    List<Integer> numbers = this.parseFile(day1InputFile);
    for (Integer i = 0; i < numbers.size(); i++) {
      for (Integer j = i + 1; j < numbers.size(); j++) {
        if (numbers[i] + numbers[j] == 2020) {
          return numbers[i] * numbers[j];
        }
      }
    }
    return -1;
  }

  public Integer part2() {
    StaticResource day1InputFile = this.day01File.getDay01File();
    List<Integer> numbers = this.parseFile(day1InputFile);

    for (Integer i = 0; i < numbers.size(); i++) {
      for (Integer j = i + 1; j < numbers.size(); j++) {
        for (Integer k = j + 1; k < numbers.size(); k++) {
          if (numbers[i] + numbers[j] + numbers[k] == 2020) {
            return numbers[i] * numbers[j] * numbers[k];
          }
        }
      }
    }
    return -1;
  }

  private List<Integer> parseFile(StaticResource file) {
    String inputsFile = file.body.toString();
    List<Integer> numbers = new List<Integer>();
    for (String line : inputsFile.split('\n')) {
      numbers.add(Integer.valueOf(line));
    }
    return numbers;
  }

  public class Day01File implements IDay01File {
    public StaticResource getDay01File() {
      return [SELECT Id, Body FROM StaticResource WHERE Name = 'Day1Input'];
    }
  }
}
