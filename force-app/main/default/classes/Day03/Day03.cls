public with sharing class Day03 {
    IStaticResourceRepository staticResourceRepository;
    public Day03() {
        this(new StaticResourceRepository());
    }
    public Day03(IStaticResourceRepository staticResourceRepository) {
        this.staticResourceRepository = staticResourceRepository;
    }

    public Integer part1() {
        StaticResource day3InputFile = this.staticResourceRepository.getStaticResourceByName(
            'Day3Input'
        );
        List<List<String>> forest = this.parseFile(day3InputFile);
        return this.countTrees(forest, 1, 3);
    }

    public Integer part2() {
        StaticResource day3InputFile = this.staticResourceRepository.getStaticResourceByName(
            'Day3Input'
        );
        List<List<String>> forest = this.parseFile(day3InputFile);
        return this.countTrees(forest, 1, 1) *
            this.countTrees(forest, 1, 3) *
            this.countTrees(forest, 1, 5) *
            this.countTrees(forest, 1, 7) *
            this.countTrees(forest, 2, 1);
    }

    private List<List<String>> parseFile(StaticResource file) {
        String inputsFile = file.body.toString();
        List<List<String>> forest = new List<List<String>>();
        for (String line : inputsFile.split('\n')) {
            forest.add(new List<String>(line.split('')));
        }
        return forest;
    }

    private Integer countTrees(
        List<List<String>> forest,
        Integer movY,
        Integer movX
    ) {
        Integer trees = 0;
        Integer y = movY;
        Integer x = movX;
        Integer forestWidth = forest[0].size();
        do {
            if ('#'.equals(forest[y][x])) {
                trees++;
            }
            y += movY;
            x = math.mod(x + movX, forestWidth);
        } while (y < forest.size());
        return trees;
    }
}
