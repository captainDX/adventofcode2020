public with sharing class Day08 {
  IStaticResourceRepository staticResourceRepository;
  public Day08() {
    this(new StaticResourceRepository());
  }
  public Day08(IStaticResourceRepository staticResourceRepository) {
    this.staticResourceRepository = staticResourceRepository;
  }

  public Integer part1() {
    StaticResource day8InputFile = this.staticResourceRepository.getStaticResourceByName(
      'Day8Input'
    );
    List<Simulator.Instruction> instructions = this.parseFile(day8InputFile);
    Simulator simulator = new Simulator(instructions);
    Simulator.Result result = simulator.run();
    return result.acc;
  }

  public void part2() {
    StaticResource day8InputFile = this.staticResourceRepository.getStaticResourceByName(
      'Day8Input'
    );
    List<Simulator.Instruction> instructions = this.parseFile(day8InputFile);
    Database.executeBatch(new Simulator(instructions));
  }

  private List<Simulator.Instruction> parseFile(StaticResource file) {
    String inputsFile = file.body.toString();
    List<Simulator.Instruction> instructions = new List<Simulator.Instruction>();
    for (String line : inputsFile.split('\n')) {
      List<String> instructionParts = line.split(' ');
      String command = instructionParts[0];
      Integer argument = Integer.valueOf(instructionParts[1]);
      switch on command {
        when 'nop' {
          instructions.add(new Simulator.Nop(argument));
        }
        when 'acc' {
          instructions.add(new Simulator.Acc(argument));
        }
        when 'jmp' {
          instructions.add(new Simulator.Jmp(argument));
        }
      }
    }
    return instructions;
  }
}
