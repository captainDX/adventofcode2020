public class Simulator implements Database.Batchable<Integer>, Database.Stateful {
  private List<Instruction> instructions;
  public Simulator(List<Instruction> instructions) {
    this.instructions = instructions;
  }
  private Integer pc;
  private Integer acc;
  public Result run() {
    this.pc = 0;
    this.acc = 0;
    Boolean infiniteLoop = false;
    Set<Integer> instructionsExecuted = new Set<Integer>();
    do {
      if (!instructionsExecuted.contains(pc)) {
        instructionsExecuted.add(pc);
        this.decodeAndExecute(this.instructions[pc]);
      } else {
        infiniteLoop = true;
      }
    } while (!infiniteLoop && pc < this.instructions.size());
    return new Result(acc, infiniteLoop);
  }

  public Iterable<Integer> start(Database.BatchableContext info) {
    List<Integer> iterations = new List<Integer>();
    for (Integer i = 0; i < this.instructions.size(); i++) {
      iterations.add(i);
    }
    return iterations;
  }

  public void execute(Database.BatchableContext bc, List<Integer> iterations) {
    for (Integer i : iterations) {
      Instruction savedInstruction = this.instructions[i];
      Instruction fixedInstruction;
      if (savedInstruction instanceof Nop) {
        fixedInstruction = new Jmp(savedInstruction.argument);
      } else if (savedInstruction instanceof Jmp) {
        fixedInstruction = new Nop(savedInstruction.argument);
      } else {
        continue;
      }
      this.instructions[i] = fixedInstruction;
      Result result = this.run();
      if (!result.infiniteLoop) {
        System.debug(result.acc);
        System.abortJob(bc.getJobId());
      } else {
        this.instructions[i] = savedInstruction;
      }
    }
  }

  public void finish(Database.BatchableContext info) {
  }

  private void decodeAndExecute(Instruction instruction) {
    if (instruction instanceof Acc) {
      this.acc += instruction.argument;
      this.pc++;
    }
    if (instruction instanceof Jmp) {
      this.pc += instruction.argument;
    }
    if (instruction instanceof Nop) {
      this.pc++;
    }
  }

  public class Result {
    public Integer acc;
    public Boolean infiniteLoop;
    public Result(Integer acc, Boolean infiniteLoop) {
      this.acc = acc;
      this.infiniteLoop = infiniteLoop;
    }
  }

  public abstract class Instruction {
    public Integer argument;
    public Instruction(Integer argument) {
      this.argument = argument;
    }
  }

  public class Nop extends Instruction {
    public Nop(Integer argument) {
      super(argument);
    }
  }

  public class Acc extends Instruction {
    public Acc(Integer argument) {
      super(argument);
    }
  }

  public class Jmp extends Instruction {
    public Jmp(Integer argument) {
      super(argument);
    }
  }
}
