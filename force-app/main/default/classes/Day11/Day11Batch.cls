public with sharing class Day11Batch implements Database.batchable<Integer>, Database.Stateful {
  IStaticResourceRepository staticResourceRepository;
  String part;
  public Day11Batch(String part) {
    this(new StaticResourceRepository(), part);
  }
  public Day11Batch(
    IStaticResourceRepository staticResourceRepository,
    String part
  ) {
    this.staticResourceRepository = staticResourceRepository;
    this.part = part;
  }

  Ferry ferry;

  public Iterable<Integer> start(Database.BatchableContext info) {
    StaticResource day11InputFile = this.staticResourceRepository.getStaticResourceByName(
      'Day11Input'
    );
    List<List<String>> seats = this.parse(day11InputFile);
    if (part == 'part1') {
      ferry = new Ferry(seats, true, 4, 'part1');
    }
    if (part == 'part2') {
      ferry = new Ferry(seats, false, 5, 'part2');
    }
    List<Integer> listToReturn = new List<Integer>();
    for (Integer i = 0; i < 200; i++) {
      listToReturn.add(i);
    }
    return listToReturn;
  }

  public void execute(
    Database.BatchableContext info,
    List<Integer> iterations
  ) {
    Boolean finished = this.ferry.calculateOccupiedSeats(iterations);
    if (finished) {
      System.abortJob(info.getJobId());
    }
  }
  public void finish(Database.BatchableContext info) {
  }

  private List<List<String>> parse(StaticResource file) {
    String inputsFile = file.body.toString();
    List<List<String>> seats = new List<List<String>>();
    for (String line : inputsFile.split('\n')) {
      seats.add(new List<String>(line.split('')));
    }
    return seats;
  }

  private class Ferry {
    private List<List<String>> seats;
    private Boolean onlyOneDistance;
    private Integer ruleOccupied;
    private String resultName;

    public Ferry(
      List<List<String>> seats,
      Boolean onlyOneDistance,
      Integer ruleOccupied,
      String resultName
    ) {
      this.seats = seats;
      this.onlyOneDistance = onlyOneDistance;
      this.ruleOccupied = ruleOccupied;
      this.resultName = resultName;
    }

    public Boolean calculateOccupiedSeats(List<Integer> iterations) {
      for (Integer iteration : iterations) {
        Boolean changed;
        changed = false;
        List<List<String>> nextSeats = new List<List<String>>();
        for (Integer i = 0; i < this.seats.size(); i++) {
          nextSeats.add(new List<String>(this.seats[i]));
          for (Integer j = 0; j < this.seats[i].size(); j++) {
            nextSeats[i][j] = this.nextValue(i, j);
            if (changed == false && this.seats[i][j] != nextSeats[i][j]) {
              changed = true;
            }
          }
        }
        this.seats = nextSeats;
        if (!changed) {
          String allSeats;
          for (List<String> row : this.seats) {
            allSeats = allSeats + String.join(row, '');
          }
          insert new Result__c(
            Name = this.resultName,
            Day__c = 11,
            Number__c = allSeats.countMatches('#')
          );
          return true;
        }
      }
      return false;
    }

    private Boolean isLeftBorder(Integer j) {
      return j == 0;
    }

    private Boolean isTopBorder(Integer i) {
      return i == 0;
    }

    private Boolean isRightBorder(Integer j) {
      return j == this.seats[0].size() - 1;
    }

    private Boolean isBottomBorder(Integer i) {
      return i == this.seats.size() - 1;
    }

    private String nextValue(Integer i, Integer j) {
      if (this.seats[i][j] == 'L') {
        if (this.allFreeAround(i, j)) {
          return '#';
        } else {
          return 'L';
        }
      } else if (this.seats[i][j] == '#') {
        if (this.maxAroundOccupied(i, j)) {
          return 'L';
        } else {
          return '#';
        }
      }
      return this.seats[i][j];
    }

    private InteratorBoundaries getInteratorBoundaries(Integer i, Integer j) {
      InteratorBoundaries result = new InteratorBoundaries();
      if (this.isLeftBorder(j)) {
        result.minX = 0;
      }
      if (this.isTopBorder(i)) {
        result.minY = 0;
      }
      if (this.isRightBorder(j)) {
        result.maxX = 0;
      }
      if (this.isBottomBorder(i)) {
        result.maxY = 0;
      }
      return result;
    }

    private Boolean allFreeAround(Integer i, Integer j) {
      InteratorBoundaries boundaries = this.getInteratorBoundaries(i, j);
      for (Integer x = boundaries.minX; x <= boundaries.maxX; x++) {
        for (Integer y = boundaries.minY; y <= boundaries.maxY; y++) {
          if (x == 0 && y == 0) {
            continue;
          }
          Integer distance = 1;
          Integer row = i + y * distance;
          Integer column = j + x * distance;
          while (
            row >= 0 &&
            row < this.seats.size() &&
            column >= 0 &&
            column < this.seats[0].size() &&
            (!this.onlyOneDistance || distance == 1)
          ) {
            String seat = this.seats[row][column];
            if (seat == '#') {
              return false;
            }
            if (seat == 'L') {
              break;
            }
            distance++;
            row = i + y * distance;
            column = j + x * distance;
          }
        }
      }
      return true;
    }

    private Boolean maxAroundOccupied(Integer i, Integer j) {
      Integer aroundOccupiedCount = 0;
      InteratorBoundaries boundaries = this.getInteratorBoundaries(i, j);
      for (Integer x = boundaries.minX; x <= boundaries.maxX; x++) {
        for (Integer y = boundaries.minY; y <= boundaries.maxY; y++) {
          if (x == 0 && y == 0) {
            continue;
          }
          Integer distance = 1;
          Integer row = i + y * distance;
          Integer column = j + x * distance;
          while (
            row >= 0 &&
            row < this.seats.size() &&
            column >= 0 &&
            column < this.seats[0].size() &&
            (!this.onlyOneDistance || distance == 1)
          ) {
            String seat = this.seats[row][column];
            // if (i == 0 && j == 2) {
            //   System.debug(distance);
            //   System.debug(i + ':' + y);
            //   System.debug(j + ':' + x);
            //   System.debug(seat);
            // }
            if (seat == '#') {
              aroundOccupiedCount++;
              // if (i == 0 && j == 2) {
              //   System.debug(aroundOccupiedCount + '>=' + this.ruleOccupied);
              // }
              if (aroundOccupiedCount >= this.ruleOccupied) {
                return true;
              }
              break;
            }
            if (seat == 'L') {
              break;
            }
            distance++;
            row = i + y * distance;
            column = j + x * distance;
          }
        }
      }
      return false;
    }
  }

  private class InteratorBoundaries {
    public Integer minX;
    public Integer maxX;
    public Integer minY;
    public Integer maxY;
    public InteratorBoundaries() {
      this.minX = -1;
      this.maxX = 1;
      this.minY = -1;
      this.maxY = 1;
    }
  }
}
