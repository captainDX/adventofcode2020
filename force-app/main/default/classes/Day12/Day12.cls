public with sharing class Day12 {
  IStaticResourceRepository staticResourceRepository;
  public Day12() {
    this(new StaticResourceRepository());
  }
  public Day12(IStaticResourceRepository staticResourceRepository) {
    this.staticResourceRepository = staticResourceRepository;
  }

  public Integer part1() {
    StaticResource day12InputFile = this.staticResourceRepository.getStaticResourceByName(
      'Day12Input'
    );
    Ferry ferry = new Ferry();
    String steps = day12InputFile.body.toString();
    for (String step : steps.split('\n')) {
      ferry.move(step.substring(0, 1), Integer.valueOf(step.substring(1)));
    }
    return ferry.getManhattanDistance();
  }

  public Integer part2() {
    StaticResource day12InputFile = this.staticResourceRepository.getStaticResourceByName(
      'Day12Input'
    );
    Ferry ferry = new Ferry(new Waypoint());
    String steps = day12InputFile.body.toString();
    for (String step : steps.split('\n')) {
      ferry.move2(step.substring(0, 1), Integer.valueOf(step.substring(1)));
    }
    return ferry.getManhattanDistance();
  }

  private class Ferry {
    Integer x;
    Integer y;
    String direction;

    public Ferry() {
      this.x = 0;
      this.y = 0;
      this.direction = 'E';
    }

    public void move(String action, Integer amount) {
      switch on action {
        when 'N' {
          y += amount;
        }
        when 'S' {
          y -= amount;
        }
        when 'E' {
          x += amount;
        }
        when 'W' {
          x -= amount;
        }
        when 'L', 'R' {
          this.changeDirection(action, amount);
        }
        when 'F' {
          this.move(this.direction, amount);
        }
      }
    }

    Waypoint waypoint;
    public Ferry(Waypoint waypoint) {
      this.x = 0;
      this.y = 0;
      this.waypoint = waypoint;
    }

    public void move2(String action, Integer amount) {
      switch on action {
        when 'N', 'S', 'E', 'W', 'L', 'R' {
          this.waypoint.move(action, amount);
        }
        when 'F' {
          this.moveFerry(amount);
        }
      }
      System.debug('x:' + this.x + ' - y:' + this.y);
      System.debug('Wx:' + this.waypoint.x + ' - Wy:' + this.waypoint.y);
    }

    private void moveFerry(Integer amount) {
      this.x += (this.waypoint.x * amount);
      this.y += (this.waypoint.y * amount);
    }

    private void changeDirection(String leftOrRight, Integer amount) {
      List<String> directions = new List<String>{ 'E', 'S', 'W', 'N' };
      Integer currentDirection = directions.indexOf(this.direction);
      Integer turns = amount / 90;
      if (leftOrRight == 'L') {
        turns = turns * -1;
      }
      this.direction = directions[Math.mod(currentDirection + turns + 4, 4)];
    }

    public Integer getManhattanDistance() {
      return Math.abs(this.x) + Math.abs(y);
    }
  }

  private class Waypoint {
    public Integer x;
    public Integer y;

    public Waypoint() {
      this(10, 1);
    }

    public Waypoint(Integer x, Integer y) {
      this.x = x;
      this.y = y;
    }

    public void move(String action, Integer amount) {
      switch on action {
        when 'N' {
          y += amount;
        }
        when 'S' {
          y -= amount;
        }
        when 'E' {
          x += amount;
        }
        when 'W' {
          x -= amount;
        }
        when 'L' {
          Decimal rads = -1 * amount * Math.pi / 180;
          Integer newX = Math.round(x * Math.cos(rads) + y * Math.sin(rads));
          Integer newY = Math.round(y * Math.cos(rads) - x * Math.sin(rads));
          x = newX;
          y = newY;
        }
        when 'R' {
          Decimal rads = amount * Math.pi / 180;
          Integer newX = Math.round(x * Math.cos(rads) + y * Math.sin(rads));
          Integer newY = Math.round(y * Math.cos(rads) - x * Math.sin(rads));
          x = newX;
          y = newY;
        }
      }
    }
  }
}
