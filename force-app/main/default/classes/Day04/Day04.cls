public with sharing class Day04 {
  IStaticResourceRepository staticResourceRepository;
  public Day04() {
    this(new StaticResourceRepository());
  }
  public Day04(IStaticResourceRepository staticResourceRepository) {
    this.staticResourceRepository = staticResourceRepository;
  }

  public Integer part1() {
    StaticResource day4InputFile = this.staticResourceRepository.getStaticResourceByName(
      'Day4Input'
    );
    List<String> passports = this.parseFile(day4InputFile);
    Integer countValidPassports = 0;
    for (String passport : passports) {
      if (new Passport(passport).hasRequiredFields()) {
        countValidPassports++;
      }
    }
    return countValidPassports;
  }

  public Integer part2() {
    StaticResource day4InputFile = this.staticResourceRepository.getStaticResourceByName(
      'Day4Input'
    );
    List<String> passports = this.parseFile(day4InputFile);
    Integer countValidPassports = 0;
    for (String passport : passports) {
      if (new Passport(passport).isValid()) {
        countValidPassports++;
      }
    }
    return countValidPassports;
  }

  public List<String> parseFile(StaticResource file) {
    String inputsFile = file.body.toString();
    List<String> passports = new List<String>();
    for (String passport : inputsFile.split('\n\n')) {
      passports.add(passport);
    }
    return passports;
  }

  private class Passport {
    BirthYear birthyear;
    IssueYear issueYear;
    ExpirationYear expirationYear;
    Height height;
    HairColor hairColor;
    EyeColor eyeColor;
    PassportID passportId;
    public Passport(String passport) {
      List<String> parts = passport.split('[\\n\\s]');
      for (String part : parts) {
        List<String> field = part.split(':');
        switch on field[0] {
          when 'byr' {
            this.birthyear = new BirthYear(field[1]);
          }
          when 'iyr' {
            this.issueYear = new IssueYear(field[1]);
          }
          when 'eyr' {
            this.expirationYear = new ExpirationYear(field[1]);
          }
          when 'hgt' {
            this.height = new Height(field[1]);
          }
          when 'hcl' {
            this.hairColor = new HairColor(field[1]);
          }
          when 'ecl' {
            this.eyeColor = new EyeColor(field[1]);
          }
          when 'pid' {
            this.passportId = new PassportID(field[1]);
          }
        }
      }
    }

    public Boolean hasRequiredFields() {
      return this.birthyear != null &&
        this.issueYear != null &&
        this.expirationYear != null &&
        this.height != null &&
        this.hairColor != null &&
        this.eyeColor != null &&
        this.passportId != null;
    }

    public Boolean isValid() {
      return this.hasRequiredFields() &&
        this.birthyear.isValid() &&
        this.issueYear.isValid() &&
        this.expirationYear.isValid() &&
        this.height.isValid() &&
        this.hairColor.isValid() &&
        this.eyeColor.isValid() &&
        this.passportId.isValid();
    }
  }

  private abstract class Year {
    Integer year;
    public Year(String year) {
      this.year = Integer.valueOf(year);
    }
  }

  private class BirthYear extends Year {
    public BirthYear(String year) {
      super(year);
    }
    public Boolean isValid() {
      return this.year >= 1920 && this.year <= 2002;
    }
  }

  private class IssueYear extends Year {
    public IssueYear(String year) {
      super(year);
    }
    public Boolean isValid() {
      return this.year >= 2010 && this.year <= 2020;
    }
  }

  private class ExpirationYear extends Year {
    public ExpirationYear(String year) {
      super(year);
    }
    public Boolean isValid() {
      return this.year >= 2020 && this.year <= 2030;
    }
  }

  private class Height {
    String height;
    public Height(String height) {
      this.height = height;
    }

    public Boolean isValid() {
      Pattern heightPattern = pattern.compile('(^\\d+)(cm|in)');
      Matcher heightMatcher = heightPattern.matcher(this.height);
      if (
        heightMatcher.matches() &&
        heightMatcher.groupCount() == 2 &&
        heightMatcher.group(1).isNumeric()
      ) {
        Integer height = Integer.valueOf(heightMatcher.group(1));
        if (heightMatcher.group(2) == 'cm') {
          return height >= 150 && height <= 193;
        }

        if (heightMatcher.group(2) == 'in') {
          return height >= 59 && height <= 76;
        }
      }
      return false;
    }
  }

  private class HairColor {
    private String hairColor;
    public HairColor(String hairColor) {
      this.hairColor = hairColor;
    }

    public Boolean isValid() {
      Pattern hairColorPattern = pattern.compile('^#([a-fA-F0-9]{6})$');
      return hairColorPattern.matcher(this.hairColor).matches();
    }
  }

  private class EyeColor {
    private String eyeColor;
    public EyeColor(String eyeColor) {
      this.eyeColor = eyeColor;
    }

    public Boolean isValid() {
      Pattern eyeColorPattern = pattern.compile('amb|blu|brn|gry|grn|hzl|oth');
      return eyeColorPattern.matcher(this.eyeColor).matches();
    }
  }

  private class PassportID {
    private String passportId;
    public PassportID(String passportId) {
      this.passportId = passportId;
    }

    public Boolean isValid() {
      Pattern passportIdPattern = pattern.compile('\\d{9}');
      return passportIdPattern.matcher(this.passportId).matches();
    }
  }
}
