public interface IStaticResourceRepository {
    StaticResource getStaticResourceByName(String name);
}
