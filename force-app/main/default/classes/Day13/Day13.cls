public with sharing class Day13 {
  IStaticResourceRepository staticResourceRepository;
  public Day13() {
    this(new StaticResourceRepository());
  }
  public Day13(IStaticResourceRepository staticResourceRepository) {
    this.staticResourceRepository = staticResourceRepository;
  }

  public Integer part1() {
    StaticResource day13InputFile = this.staticResourceRepository.getStaticResourceByName(
      'Day13Input'
    );
    List<String> parts = day13InputFile.body.toString().split('\n');
    Integer minute = Integer.valueOf(parts[0]);
    Integer minWait = minute;
    Integer busToCatch;
    for (String bus : parts[1].split(',')) {
      if (bus == 'x') {
        continue;
      }

      Integer busNumber = Integer.valueOf(bus);
      Integer firstTimeAvailable = (minute / busNumber + 1) * busNumber;
      if ((firstTimeAvailable - minute) < minWait) {
        minWait = firstTimeAvailable - minute;
        busToCatch = busNumber;
      }
    }
    return busToCatch * minWait;
  }

  List<Bus> xBuses = new List<Bus>();
  public Long part2() {
    StaticResource day13InputFile = this.staticResourceRepository.getStaticResourceByName(
      'Day13Input'
    );
    List<String> data = day13InputFile.body.toString().split('\n')[1]
      .split(',');
    List<Bus> buses = new List<Bus>();
    for (Integer i = 0; i < data.size(); i++) {
      if (data[i] == 'x') {
        continue;
      }
      buses.add(new Bus(Integer.valueOf(data[i]), i));
    }

    Bus busX = buses[0];
    xBuses.add(new Bus(1, 0));
    Long x;
    for (Integer i = 1; i < buses.size(); i++) {
      Integer n = 0;
      Bus busY = buses[i];
      x = calculateX(n);
      Long y = ((busX.frequency * x) + busY.minutes) / busY.frequency;
      while (busX.frequency * x != busY.frequency * y - busY.minutes) {
        n++;
        x = calculateX(n);
        y = ((busX.frequency * x) + busY.minutes) / busY.frequency;
      }
      xBuses.add(new Bus(busY.frequency, n));
    }

    return busX.frequency * x;
  }

  private Long calculateX(Integer n) {
    Bus lastBus = xBuses[xBuses.size() - 1];
    Long result = lastBus.frequency * n + lastBus.minutes;
    for (Integer i = xBuses.size() - 2; i >= 0; i--) {
      result = xBuses[i].frequency * result + xBuses[i].minutes;
    }
    return result;
  }

  private class Bus {
    Integer frequency;
    Integer minutes;

    public Bus(Integer frequency, Integer minutes) {
      this.frequency = frequency;
      this.minutes = minutes;
    }
  }
}
