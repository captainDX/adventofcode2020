public with sharing class Day02 {
  IStaticResourceRepository staticResourceRepository;
  public Day02() {
    this(new StaticResourceRepository());
  }
  public Day02(IStaticResourceRepository staticResourceRepository) {
    this.staticResourceRepository = staticResourceRepository;
  }

  public Integer part1() {
    IPolicy policy = new PolicyPart1();
    return this.validatePasswords(policy);
  }

  public Integer part2() {
    IPolicy policy = new PolicyPart2();
    return this.validatePasswords(policy);
  }

  private Integer validatePasswords(IPolicy policy) {
    StaticResource day2InputFile = this.staticResourceRepository.getStaticResourceByName(
      'Day2Input'
    );
    List<PasswordPolicy> passwordPolicies = this.parseFile(day2InputFile);

    Integer validPasswords = 0;
    for (PasswordPolicy passwordPolicy : PasswordPolicies) {
      if (
        policy.isValid(passwordPolicy.policyConfig, passwordPolicy.password)
      ) {
        validPasswords++;
      }
    }
    return validPasswords;
  }

  private List<PasswordPolicy> parseFile(StaticResource file) {
    String inputsFile = file.body.toString();
    List<PasswordPolicy> passwordPolicies = new List<PasswordPolicy>();
    for (String line : inputsFile.split('\n')) {
      passwordPolicies.add(createPasswordPolicyFromLine(line));
    }
    return passwordPolicies;
  }

  private static PasswordPolicy createPasswordPolicyFromLine(String line) {
    Integer fistNumber = Integer.valueOf(line.substring(0, line.indexOf('-')));
    Integer secondNumber = Integer.valueOf(
      line.substring(line.indexOf('-') + 1, line.indexOf(' '))
    );
    String letter = line.substring(line.indexOf(' ') + 1, line.indexOf(':'));
    PolicyConfig policyConfig = new PolicyConfig(
      fistNumber,
      secondNumber,
      letter
    );
    Password password = new Password(line.substring(line.indexOf(':') + 2));
    return new PasswordPolicy(policyConfig, password);
  }

  private class PasswordPolicy {
    public PolicyConfig policyConfig;
    public Password password;

    public PasswordPolicy(PolicyConfig policyConfig, Password password) {
      this.policyConfig = policyConfig;
      this.password = password;
    }
  }

  private class Password {
    private String password;
    public Password(String password) {
      this.password = password;
    }

    public String char(Integer i) {
      return this.password.subString(i, i + 1);
    }

    public Integer length() {
      return this.password.length();
    }
  }

  private class PolicyConfig {
    private Integer firstNumber;
    private Integer secondNumber;
    private String letter;

    public PolicyConfig(
      Integer firstNumber,
      Integer secondNumber,
      String letter
    ) {
      this.firstNumber = firstNumber;
      this.secondNumber = secondNumber;
      this.letter = letter;
    }
  }

  private interface IPolicy {
    Boolean isValid(PolicyConfig policyConfig, Password password);
  }

  private class PolicyPart1 implements IPolicy {
    public Boolean isValid(PolicyConfig policyConfig, Password password) {
      Integer occurrences = 0;
      for (Integer i = 0; i < password.length(); i++) {
        if (policyConfig.letter.equals(password.char(i))) {
          occurrences++;
        }
      }
      return occurrences >= policyConfig.firstNumber &&
        occurrences <= policyConfig.secondNumber;
    }
  }

  private class PolicyPart2 implements IPolicy {
    public Boolean isValid(PolicyConfig policyConfig, Password password) {
      return policyConfig.letter.equals(
          password.char(policyConfig.firstNumber - 1)
        ) ^
        policyConfig.letter.equals(
          password.char(policyConfig.secondNumber - 1)
        );
    }
  }
}
