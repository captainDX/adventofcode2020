public with sharing class Day05 {
  IStaticResourceRepository staticResourceRepository;
  public Day05() {
    this(new StaticResourceRepository());
  }
  public Day05(IStaticResourceRepository staticResourceRepository) {
    this.staticResourceRepository = staticResourceRepository;
  }

  public Integer part1() {
    StaticResource day5InputFile = this.staticResourceRepository.getStaticResourceByName(
      'Day5Input'
    );
    String seats = day5InputFile.body.toString();
    Integer maxSeatID = 0;
    for (String seat : seats.split('\n')) {
      Integer seatID = this.calculateSeatID(seat);
      if (seatID > maxSeatID) {
        maxSeatID = seatID;
      }
    }
    return maxSeatID;
  }

  public Integer part2() {
    StaticResource day5InputFile = this.staticResourceRepository.getStaticResourceByName(
      'Day5Input'
    );
    String seats = day5InputFile.body.toString();
    List<Integer> seatIDs = new List<Integer>();
    for (String seat : seats.split('\n')) {
      seatIDs.add(this.calculateSeatID(seat));
    }
    seatIDs.sort();
    for (Integer i = 0; i < seatIDs.size(); i++) {
      if (seatIDs[i + 1] - seatIDs[i] > 1) {
        return seatIDs[i] + 1;
      }
    }
    return null;
  }

  private Integer calculateSeatID(String seat) {
    List<String> seatParts = seat.split('[RL]');
    return this.binaryToInteger(
      seat.replace('F', '0')
        .replace('B', '1')
        .replace('L', '0')
        .replace('R', '1')
    );
  }

  private Integer binaryToInteger(String value) {
    Integer result = 0;
    for (String s : value.split('')) {
      result *= 2;
      result += (s == '1' ? 1 : 0);
    }
    return result;
  }
}
