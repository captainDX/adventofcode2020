public with sharing class Day09 {
  IStaticResourceRepository staticResourceRepository;
  public Day09() {
    this(new StaticResourceRepository());
  }
  public Day09(IStaticResourceRepository staticResourceRepository) {
    this.staticResourceRepository = staticResourceRepository;
  }

  public Long part1(Integer preambleLength) {
    StaticResource day9InputFile = this.staticResourceRepository.getStaticResourceByName(
      'day9Input'
    );
    XMASCracker xmas = new XMASCracker(
      day9InputFile.body.toString(),
      preambleLength
    );
    return xmas.firstInvalid();
  }

  public Long part2(Integer preambleLength) {
    StaticResource day9InputFile = this.staticResourceRepository.getStaticResourceByName(
      'day9Input'
    );
    XMASCracker xmas = new XMASCracker(
      day9InputFile.body.toString(),
      preambleLength
    );
    return xmas.encryptionWeakness();
  }

  private class XMASCracker {
    private List<Long> data;
    Integer preambleLength;
    public XMASCracker(String file, Integer preambleLength) {
      data = new List<Long>();
      for (String line : file.split('\n')) {
        this.data.add(Long.valueOf(line));
      }
      this.preambleLength = preambleLength;
    }

    public Long firstInvalid() {
      for (Integer i = this.preambleLength; i < this.data.size(); i++) {
        if (!isValid(i)) {
          return this.data[i];
        }
      }
      return null;
    }

    public Long encryptionWeakness() {
      Long firstInvalid = this.firstInvalid();
      for (Integer i = 0; i < this.data.size(); i++) {
        Long encryptionWeaknessTest = this.data[i];
        List<Long> numbersUsed = new List<Long>{ encryptionWeaknessTest };
        for (Integer j = i + 1; j < this.data.size(); j++) {
          encryptionWeaknessTest += this.data[j];
          numbersUsed.add(this.data[j]);
          if (encryptionWeaknessTest == firstInvalid) {
            numbersUsed.sort();
            return numbersUsed[0] + numbersUsed[numbersUsed.size() - 1];
          }
          if (encryptionWeaknessTest > firstInvalid) {
            break;
          }
        }
      }
      return null;
    }

    private Boolean isValid(Integer index) {
      Long valueToFind = this.data[index];
      for (Integer j = index - this.preambleLength; j < index; j++) {
        for (Integer k = j + 1; k < index + this.preambleLength; k++) {
          if (this.data[j] + this.data[k] == valueToFind) {
            return true;
          }
        }
      }
      return false;
    }
  }
}
