public with sharing class Day10 {
  IStaticResourceRepository staticResourceRepository;
  public Day10() {
    this(new StaticResourceRepository());
  }
  public Day10(IStaticResourceRepository staticResourceRepository) {
    this.staticResourceRepository = staticResourceRepository;
  }

  public Long part1() {
    StaticResource day10InputFile = this.staticResourceRepository.getStaticResourceByName(
      'day10Input'
    );
    JoltsCalculator calculator = new JoltsCalculator(
      day10InputFile.body.toString()
    );
    return calculator.calculateDifference();
  }

  public Long part2() {
    StaticResource day10InputFile = this.staticResourceRepository.getStaticResourceByName(
      'day10Input'
    );
    JoltsCalculator calculator = new JoltsCalculator(
      day10InputFile.body.toString()
    );
    return calculator.calculateArrangements();
  }

  private class JoltsCalculator {
    private List<Long> jolts;
    public JoltsCalculator(String inputFile) {
      jolts = new List<Long>();
      for (String line : inputFile.split('\n')) {
        jolts.add(Long.valueOf(line));
      }
    }

    public Long calculateDifference() {
      this.jolts.sort();
      Long previousJolt = 0;
      Long oneJoltDifference = 0;
      Long threeJoltDifference = 1;
      for (Long jolt : this.jolts) {
        if (jolt - previousJolt == 1) {
          oneJoltDifference++;
        }
        if (jolt - previousJolt == 3) {
          threeJoltDifference++;
        }
        previousJolt = jolt;
      }
      return oneJoltDifference * threeJoltDifference;
    }

    public Long calculateArrangements() {
      this.jolts.sort();
      this.jolts.add(this.jolts[this.jolts.size() - 1] + 3);
      Map<Long, Long> solution = new Map<Long, Long>{ 0L => 1L };
      for (Long jolt : jolts) {
        solution.put(jolt, 0);
        if (solution.keySet().contains(jolt - 1)) {
          solution.put(jolt, solution.get(jolt) + solution.get(jolt - 1));
        }
        if (solution.keySet().contains(jolt - 2)) {
          solution.put(jolt, solution.get(jolt) + solution.get(jolt - 2));
        }
        if (solution.keySet().contains(jolt - 3)) {
          solution.put(jolt, solution.get(jolt) + solution.get(jolt - 3));
        }
      }
      return solution.get(jolts[jolts.size() - 1]);
    }
  }
}
