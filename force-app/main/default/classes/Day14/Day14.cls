public with sharing class Day14 {
  IStaticResourceRepository staticResourceRepository;
  public Day14() {
    this(new StaticResourceRepository());
  }
  public Day14(IStaticResourceRepository staticResourceRepository) {
    this.staticResourceRepository = staticResourceRepository;
  }

  List<String> zero;

  public Long part1() {
    this.zero = new List<String>();
    for (Integer i = 0; i < 36; i++) {
      zero.add('0');
    }
    StaticResource day14InputFile = this.staticResourceRepository.getStaticResourceByName(
      'Day14Input'
    );
    List<String> lines = day14InputFile.body.toString().split('\n');
    List<String> mask;
    Map<String, String> memory = new Map<String, String>();
    Pattern memoryPattern = Pattern.compile('mem\\[(\\d+)\\] = (\\d+)');

    for (String line : lines) {
      if (line.contains('mask')) {
        mask = line.split(' = ')[1].split('');
        continue;
      }
      Matcher memoryPatternMatcher = memoryPattern.matcher(line);
      memoryPatternMatcher.matches();
      String memoryKey = memoryPatternMatcher.group(1);
      String memoryValue = this.applyMask(
        mask,
        this.longToBinary(Long.valueOf(memoryPatternMatcher.group(2)))
      );
      memory.put(memoryKey, memoryValue);
    }

    Long result = 0;
    for (String value : memory.values()) {
      result += binaryToLong(value);
    }
    return result;
  }

  private String applyMask(List<String> mask, String value) {
    List<String> valueCharacters = value.split('');
    for (Integer i = 0; i < mask.size(); i++) {
      if (mask[i] == 'X') {
        continue;
      }
      valueCharacters[i] = mask[i];
    }
    return String.join(valueCharacters, '');
  }

  private String longToBinary(Long value) {
    List<String> result = this.zero.clone();
    for (Integer i = 0; i < 36; i++) {
      result[35 - i] = String.valueOf((value >> i & 1));
    }
    return String.join(result, '');
  }

  private Long binaryToLong(String value) {
    Long result = 0;
    for (String s : value.split('')) {
      result *= 2;
      result += (s == '1' ? 1 : 0);
    }
    return result;
  }
}
