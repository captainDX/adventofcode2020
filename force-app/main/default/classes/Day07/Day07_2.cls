public with sharing class Day07_2 {
  IStaticResourceRepository staticResourceRepository;
  public Day07_2() {
    this(new StaticResourceRepository());
  }
  public Day07_2(IStaticResourceRepository staticResourceRepository) {
    this.staticResourceRepository = staticResourceRepository;
  }

  public Integer part1() {
    StaticResource day7InputFile = this.staticResourceRepository.getStaticResourceByName(
      'Day7Input'
    );
    Rules rules = this.parseFile(day7InputFile);
    return rules.countShinyGoldFound();
  }

  public Integer part2() {
    StaticResource day7InputFile = this.staticResourceRepository.getStaticResourceByName(
      'Day7Input'
    );
    Rules rules = this.parseFile(day7InputFile);
    return rules.countShinyGoldBags();
  }

  private Rules parseFile(StaticResource file) {
    String inputsFile = file.body.toString();
    Rules rules = new Rules();
    for (String rule : inputsFile.split('.\n')) {
      rules.addRule(rule);
    }
    return rules;
  }

  private class Rules {
    private Map<Bag, Rule> rules;
    Bag shinyGold = new Bag('shiny gold');

    public Rules() {
      // rules = new Map<Bag, Set<Bag>>();
      rules = new Map<Bag, Rule>();
    }

    public void addRule(String rule) {
      rule = rule.replaceAll(' bag[s]?', '');
      // light red contain 1 bright white, 2 muted yellow
      List<String> parts = rule.split(' contain ');
      // 0 -> light red
      // 1 -> 1 bright white, 2 muted yellow
      // for (String insideBag : parts[1].split(', ')) {
      //   if (insideBag != 'no other') {
      //     Bag bag = new Bag(insideBag.substring(2));
      //     Set<Bag> insideBags = this.rules.get(bag);
      //     if (insideBags == null) {
      //       insideBags = new Set<Bag>();
      //     }
      //     insideBags.add(new Bag(parts[0]));
      //     this.rules.put(bag, insideBags);
      //   }
      // }

      Bag bagProcessing = new Bag(parts[0]);
      Rule containingRule = this.rules.get(bagProcessing);
      if (containingRule == null) {
        containingRule = new Rule();
      }
      for (String containedBag : parts[1].split(', ')) {
        if (containedBag != 'no other') {
          Bag bag = new Bag(containedBag.substring(2));
          Integer amount = Integer.valueOf(containedBag.substring(0, 1));
          containingRule.addContaining(new ContainingRule(bag, amount));
          this.rules.put(bagProcessing, containingRule);
          Rule containedRule = this.rules.get(bag);
          if (containedRule == null) {
            containedRule = new Rule();
          }
          containedRule.addContained(new Bag(parts[0]));
          this.rules.put(bag, containedRule);
        }
      }
    }

    public Integer countShinyGoldFound() {
      Set<Bag> bags = countBags(new Bag('shiny gold'));
      return bags.size();
    }

    private Set<Bag> countBags(Bag bagToCheck) {
      Set<Bag> bagsToReturn = new Set<Bag>();
      Set<Bag> bagsToCheck = this.rules.get(bagToCheck).getContained();
      if (bagsToCheck != null) {
        bagsToReturn.addAll(bagsToCheck);
        for (Bag bag : bagsToCheck) {
          bagsToReturn.addAll(countBags(bag));
        }
      }
      return bagsToReturn;
    }

    public Integer countShinyGoldBags() {
      return countInsideBags(new ContainingRule(new Bag('shiny gold'), 1)) - 1;
    }

    private Integer countInsideBags(ContainingRule ruleToCheck) {
      Integer result = ruleToCheck.amount;
      Set<ContainingRule> rules = this.rules.get(ruleToCheck.bag)
        .getContaining();
      for (ContainingRule rule : rules) {
        result += ruleToCheck.amount * countInsideBags(rule);
      }
      return result;
    }
  }

  private class Rule {
    Set<ContainingRule> containing;
    Set<Bag> contained;
    public Rule() {
      containing = new Set<ContainingRule>();
      contained = new Set<Bag>();
    }

    public void addContaining(ContainingRule containingRule) {
      this.containing.add(containingRule);
    }

    public void addContained(Bag bag) {
      this.contained.add(bag);
    }

    public Set<Bag> getContained() {
      return this.contained;
    }

    public Set<ContainingRule> getContaining() {
      return this.containing;
    }
  }

  private class ContainingRule {
    public Bag bag;
    public Integer amount;
    public ContainingRule(Bag bag, Integer amount) {
      this.bag = bag;
      this.amount = amount;
    }
  }

  private class Bag {
    String name;

    public Bag(String name) {
      this.name = name;
    }

    public Boolean equals(Object obj) {
      Bag other = (Bag) obj;
      return this.name == other.name;
    }

    public Integer hashCode() {
      return System.hashCode(this.name);
    }
  }
}
