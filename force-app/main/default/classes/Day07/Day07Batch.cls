public with sharing class Day07Batch implements Database.batchable<Bag>, Database.Stateful {
  IStaticResourceRepository staticResourceRepository;
  Integer count;
  Rules rules;
  Bag shinyGold = new Bag('shiny gold');
  public Day07Batch() {
    this(new StaticResourceRepository());
  }
  public Day07Batch(IStaticResourceRepository staticResourceRepository) {
    this.staticResourceRepository = staticResourceRepository;
    this.count = 0;
  }

  public Iterable<Bag> start(Database.BatchableContext info) {
    StaticResource day7InputFile = this.staticResourceRepository.getStaticResourceByName(
      'Day7Input'
    );
    this.rules = this.parseFile(day7InputFile);
    return new List<Bag>(this.rules.keySet());
  }
  public void execute(Database.BatchableContext info, List<Bag> bags) {
    this.count += this.rules.result(bags);
  }
  public void finish(Database.BatchableContext info) {
    System.debug(this.count);
  }

  private Rules parseFile(StaticResource file) {
    String inputsFile = file.body.toString();
    Rules rules = new Rules();
    for (String rule : inputsFile.split('.\n')) {
      rules.addRule(rule);
    }
    return rules;
  }

  private class Rules {
    private Map<Bag, Set<Bag>> rules;
    Bag shinyGold = new Bag('shiny gold');

    public Rules() {
      rules = new Map<Bag, Set<Bag>>();
    }

    public Set<Bag> keySet() {
      return rules.keySet();
    }

    public void addRule(String rule) {
      rule = rule.replaceAll(' bag[s]?', '');
      // light red contain 1 bright white, 2 muted yellow
      List<String> parts = rule.split(' contain ');
      // 0 -> light red
      // 1 -> 1 bright white, 2 muted yellow
      Bag key = new Bag(parts[0]);
      Set<Bag> bags = rules.get(key);
      if (bags == null) {
        bags = new Set<Bag>();
      }
      for (String insideBag : parts[1].split(', ')) {
        if (insideBag != 'no other') {
          bags.add(new Bag(insideBag.substring(2)));
        }
      }
      rules.put(key, bags);
    }

    public Integer result(List<Bag> bags) {
      Set<Bag> resultBags = new Set<Bag>();
      for (Bag key : bags) {
        if (this.containsShinyGold(key, resultBags)) {
          resultBags.add(key);
        }
      }
      return resultBags.size();
    }

    private Boolean containsShinyGold(Bag key, Set<Bag> resultBags) {
      if (resultBags.contains(key)) {
        return true;
      }
      Set<Bag> rulesToCheck = this.rules.get(key);
      if (rulesToCheck.contains(this.shinyGold)) {
        return true;
      }
      Boolean result = false;
      for (Bag bag : rulesToCheck) {
        if (this.containsShinyGold(bag, resultBags)) {
          resultBags.add(bag);
          result = true;
        }
      }
      return result;
    }
  }

  private class Bag {
    String name;

    public Bag(String name) {
      this.name = name;
    }

    public Boolean equals(Object obj) {
      Bag other = (Bag) obj;
      return this.name == other.name;
    }

    public Integer hashCode() {
      return System.hashCode(this.name);
    }
  }
}
