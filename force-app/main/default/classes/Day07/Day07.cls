public with sharing class Day07 {
  IStaticResourceRepository staticResourceRepository;
  public Day07() {
    this(new StaticResourceRepository());
  }
  public Day07(IStaticResourceRepository staticResourceRepository) {
    this.staticResourceRepository = staticResourceRepository;
  }

  public void part1() {
    StaticResource day7InputFile = this.staticResourceRepository.getStaticResourceByName(
      'Day7Input'
    );
    Rules rules = this.parseFile(day7InputFile);
    System.enqueueJob(new RulesQueueable(rules));
  }

  private Rules parseFile(StaticResource file) {
    String inputsFile = file.body.toString();
    Rules rules = new Rules();
    for (String rule : inputsFile.split('.\n')) {
      rules.addRule(rule);
    }
    return rules;
  }

  private class Rules {
    private Map<Bag, Set<Bag>> rules;
    Bag shinyGold = new Bag('shiny gold');

    public Rules() {
      rules = new Map<Bag, Set<Bag>>();
    }

    public void addRule(String rule) {
      rule = rule.replaceAll(' bag[s]?', '');
      // light red contain 1 bright white, 2 muted yellow
      List<String> parts = rule.split(' contain ');
      // 0 -> light red
      // 1 -> 1 bright white, 2 muted yellow
      Bag key = new Bag(parts[0]);
      Set<Bag> bags = rules.get(key);
      if (bags == null) {
        bags = new Set<Bag>();
      }
      for (String insideBag : parts[1].split(', ')) {
        if (insideBag != 'no other') {
          bags.add(new Bag(insideBag.substring(2)));
        }
      }
      rules.put(key, bags);
    }

    public Integer countShinyGoldFound() {
      Integer result = 0;
      Set<Bag> visited = new Set<Bag>();

      for (Bag bag : rules.keySet()) {
        System.debug(bag);
        if (bag == shinyGold) {
          continue;
        }
        if (visited.contains(bag)) {
          result++;
          continue;
        }
        Set<Bag> bags = this.rules.get(bag);
        if (this.shinyGoldFound(bags)) {
          result++;
          visited.add(bag);
        }
      }
      return result;
    }

    private Boolean shinyGoldFound(Set<Bag> bags) {
      if (bags == null) {
        return false;
      }
      if (bags.contains(new Bag('shiny gold'))) {
        return true;
      }
      for (Bag bag : bags) {
        System.debug(bag);
        Set<Bag> nextBags = this.rules.get(bag);
        if (this.shinyGoldFound(nextBags)) {
          return true;
        }
      }
      return false;
    }

    public Integer result() {
      Set<Bag> resultBags = new Set<Bag>();
      for (Bag key : this.rules.keySet()) {
        if (Limits.getCpuTime() > Limits.getLimitCpuTime()) {
          System.debug(key);
          System.debug(Limits.getCpuTime());
          System.debug(resultBags.size());
        }
        if (this.containsShinyGold(key, resultBags)) {
          resultBags.add(key);
        }
      }
      return resultBags.size();
    }

    private Boolean containsShinyGold(Bag key, Set<Bag> resultBags) {
      if (resultBags.contains(key)) {
        return true;
      }
      Set<Bag> rulesToCheck = this.rules.get(key);
      if (rulesToCheck.contains(this.shinyGold)) {
        return true;
      }
      Boolean result = false;
      for (Bag bag : rulesToCheck) {
        if (this.containsShinyGold(bag, resultBags)) {
          resultBags.add(bag);
          result = true;
        }
      }
      return result;
    }
  }

  public class RulesQueueable implements Queueable {
    public Rules rules;
    public RulesQueueable(Rules rules) {
      this.rules = rules;
    }
    public void execute(QueueableContext context) {
      System.debug(rules.result());
    }
  }

  private class Bag {
    String name;

    public Bag(String name) {
      this.name = name;
    }

    public Boolean equals(Object obj) {
      Bag other = (Bag) obj;
      return this.name == other.name;
    }

    public Integer hashCode() {
      return System.hashCode(this.name);
    }
  }
}
