public with sharing class StaticResourceRepository implements IStaticResourceRepository {
    public StaticResourceRepository() {

    }

    public StaticResource getStaticResourceByName(String name) {
        return [SELECT Id, Body FROM StaticResource WHERE Name = :name];
    }

}
