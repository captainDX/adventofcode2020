public with sharing class Day06 {
  IStaticResourceRepository staticResourceRepository;
  public Day06() {
    this(new StaticResourceRepository());
  }
  public Day06(IStaticResourceRepository staticResourceRepository) {
    this.staticResourceRepository = staticResourceRepository;
  }

  public Integer part1() {
    StaticResource day6InputFile = this.staticResourceRepository.getStaticResourceByName(
      'Day6Input'
    );
    List<String> groupAnswers = this.parseFile(day6InputFile);
    Integer answeredYesQuestions = 0;
    for (String groupAnswer : groupAnswers) {
      answeredYesQuestions += new GroupAnswers(groupAnswer)
        .numberOfGroupAnswers();
    }
    return answeredYesQuestions;
  }

  public Integer part2() {
    StaticResource day6InputFile = this.staticResourceRepository.getStaticResourceByName(
      'Day6Input'
    );
    List<String> groupAnswers = this.parseFile(day6InputFile);
    Integer answeredYesQuestions = 0;
    for (String groupAnswer : groupAnswers) {
      answeredYesQuestions += new GroupAnswers(groupAnswer)
        .numberOfAllSameAnswer();
    }
    return answeredYesQuestions;
  }

  private List<String> parseFile(StaticResource file) {
    String inputsFile = file.body.toString();
    List<String> groupAnswers = new List<String>();
    for (String groupAnswer : inputsFile.split('\n\n')) {
      groupAnswers.add(groupAnswer);
    }
    return groupAnswers;
  }

  private class GroupAnswers {
    Integer groupSize;
    Map<String, Integer> answers;

    public GroupAnswers(String groupAnswers) {
      this.groupSize = 0;
      this.answers = new Map<String, Integer>();
      for (String personAnswers : groupAnswers.split('\n')) {
        this.addAnswers(personAnswers);
      }
    }

    private void addAnswers(String personAnswers) {
      this.groupSize++;
      for (String personAnswer : personAnswers.split('')) {
        Integer answeredTimes = this.answers.get(personAnswer);
        this.answers.put(
          personAnswer,
          (answeredTimes != null ? answeredTimes : 0) + 1
        );
      }
    }

    public Integer numberOfGroupAnswers() {
      return answers.size();
    }

    public Integer numberOfAllSameAnswer() {
      Integer result = 0;
      for (String key : answers.keySet()) {
        if (this.groupSize == this.answers.get(key)) {
          result++;
        }
      }
      return result;
    }
  }
}
