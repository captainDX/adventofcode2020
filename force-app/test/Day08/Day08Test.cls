@isTest
public with sharing class Day08Test {
  @isTest
  public static void itShouldReturnAcc5() {
    System.assertEquals(
      5,
      new Day08(new StaticResourceRepositoryMock()).part1()
    );
  }

  @isTest
  public static void itShouldReturnAcc8() {
    Test.startTest();
    new Day08(new StaticResourceRepositoryMock()).part2();
    Test.stopTest();
  }

  public class StaticResourceRepositoryMock implements IStaticResourceRepository {
    public StaticResource getStaticResourceByName(String name) {
      return new StaticResource(
        Body = Blob.valueOf(
          'nop +0\n' +
          'acc +1\n' +
          'jmp +4\n' +
          'acc +3\n' +
          'jmp -3\n' +
          'acc -99\n' +
          'acc +1\n' +
          'jmp -4\n' +
          'acc +6\n'
        )
      );
    }
  }
}
