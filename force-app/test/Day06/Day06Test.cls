@isTest
public with sharing class Day06Test {
  @isTest
  public static void itShouldReturn11YesAnswered() {
    System.assertEquals(
      11,
      new Day06(new StaticResourceRepositoryMock()).part1()
    );
  }

  @isTest
  public static void itShouldReturn6YesAnswered() {
    System.assertEquals(
      6,
      new Day06(new StaticResourceRepositoryMock()).part2()
    );
  }

  public class StaticResourceRepositoryMock implements IStaticResourceRepository {
    public StaticResource getStaticResourceByName(String name) {
      return new StaticResource(
        Body = Blob.valueOf(
          'abc\n' +
          '\n' +
          'a\n' +
          'b\n' +
          'c\n' +
          '\n' +
          'ab\n' +
          'ac\n' +
          '\n' +
          'a\n' +
          'a\n' +
          'a\n' +
          'a\n' +
          '\n' +
          'b\n'
        )
      );
    }
  }
}
