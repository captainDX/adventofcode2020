@isTest
public with sharing class Day01Test {
  @isTest
  public static void itShouldFindTwoNumbersThatSum2020AndMultipliedThem() {
    System.assertEquals(514579, new Day01(new Day01FileMock()).part1());
  }

  @isTest
  public static void itShouldFindThreeNumbersThatSum2020AndMultipliedThem() {
    System.assertEquals(241861950, new Day01(new Day01FileMock()).part2());
  }

  public class Day01FileMock implements Day01.IDay01File {
    public StaticResource getDay01File() {
      return new StaticResource(
        Body = Blob.valueOf('1721\n979\n366\n299\n675\n1456')
      );
    }
  }
}
