@isTest
public with sharing class Day14Test {
  @isTest
  public static void itShouldReturn295() {
    System.assertEquals(
      165L,
      new Day14(new StaticResourceRepositoryMock()).part1()
    );
  }

  public class StaticResourceRepositoryMock implements IStaticResourceRepository {
    public StaticResource getStaticResourceByName(String name) {
      return new StaticResource(
        Body = Blob.valueOf(
          'mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X\n' +
          'mem[8] = 11\n' +
          'mem[7] = 101\n' +
          'mem[8] = 0\n'
        )
      );
    }
  }
}
