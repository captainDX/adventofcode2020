@isTest
public with sharing class Day07Test {
  @isTest
  public static void itShouldReturn4TypesForShinyGold() {
    System.assertEquals(
      4,
      new Day07_2(new StaticResourceRepositoryMock()).part1()
    );
  }

  @isTest
  public static void itShouldReturn32BagsForShinyGold() {
    System.assertEquals(
      32,
      new Day07_2(new StaticResourceRepositoryMock()).part2()
    );
  }

  public class StaticResourceRepositoryMock implements IStaticResourceRepository {
    public StaticResource getStaticResourceByName(String name) {
      return new StaticResource(
        Body = Blob.valueOf(
          'light red bags contain 1 bright white bag, 2 muted yellow bags.\n' +
          'dark orange bags contain 3 bright white bags, 4 muted yellow bags.\n' +
          'bright white bags contain 1 shiny gold bag.\n' +
          'muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.\n' +
          'shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.\n' +
          'dark olive bags contain 3 faded blue bags, 4 dotted black bags.\n' +
          'vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.\n' +
          'faded blue bags contain no other bags.\n' +
          'dotted black bags contain no other bags.\n'
        )
      );
    }
  }
}
