@isTest
public with sharing class Day05Test {
  @isTest
  public static void itShouldReturnMaxSeatID820() {
    System.assertEquals(
      820,
      new Day05(new StaticResourceRepositoryMock1()).part1()
    );
  }

  @isTest
  public static void itShouldReturnSeatID102() {
    System.assertEquals(
      102,
      new Day05(new StaticResourceRepositoryMock2()).part2()
    );
  }

  public class StaticResourceRepositoryMock1 implements IStaticResourceRepository {
    public StaticResource getStaticResourceByName(String name) {
      return new StaticResource(
        Body = Blob.valueOf('BFFFBBFRRR\n' + 'FFFBBBFRRR\n' + 'BBFFBBFRLL')
      );
    }
  }

  public class StaticResourceRepositoryMock2 implements IStaticResourceRepository {
    public StaticResource getStaticResourceByName(String name) {
      return new StaticResource(
        Body = Blob.valueOf('FFFBBFFRLR\n' + 'FFFBBFFRRR\n' + 'FFFBBFFRLL')
      );
    }
  }
}
