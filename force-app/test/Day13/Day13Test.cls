@isTest
public with sharing class Day13Test {
  @isTest
  public static void itShouldReturn295() {
    System.assertEquals(
      295,
      new Day13(new StaticResourceRepositoryMock1()).part1()
    );
  }

  @isTest
  public static void itShouldReturn3417() {
    System.assertEquals(
      3417L,
      new Day13(new StaticResourceRepositoryMock2()).part2()
    );
  }

  @isTest
  public static void itShouldReturn754018() {
    System.assertEquals(
      754018L,
      new Day13(new StaticResourceRepositoryMock3()).part2()
    );
  }

  public class StaticResourceRepositoryMock1 implements IStaticResourceRepository {
    public StaticResource getStaticResourceByName(String name) {
      return new StaticResource(
        Body = Blob.valueOf('939\n' + '7,13,x,x,59,x,31,19')
      );
    }
  }

  public class StaticResourceRepositoryMock2 implements IStaticResourceRepository {
    public StaticResource getStaticResourceByName(String name) {
      return new StaticResource(Body = Blob.valueOf('939\n' + '17,x,13,19'));
    }
  }

  public class StaticResourceRepositoryMock3 implements IStaticResourceRepository {
    public StaticResource getStaticResourceByName(String name) {
      return new StaticResource(Body = Blob.valueOf('939\n' + '67,7,59,61'));
    }
  }
}
