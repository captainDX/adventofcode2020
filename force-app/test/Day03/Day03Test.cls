@isTest
public with sharing class Day03Test {
    @isTest
    public static void ItShouldReturn7Trees() {
        System.assertEquals(
            7,
            new Day03(new StaticResourceRepositoryMock()).part1()
        );
    }

    @isTest
    public static void ItShouldReturn336() {
        System.assertEquals(
            336,
            new Day03(new StaticResourceRepositoryMock()).part2()
        );
    }

    public class StaticResourceRepositoryMock implements IStaticResourceRepository {
        public StaticResource getStaticResourceByName(String name) {
            return new StaticResource(
                Body = Blob.valueOf(
                    '..##.......\n' +
                    '#...#...#..\n' +
                    '.#....#..#.\n' +
                    '..#.#...#.#\n' +
                    '.#...##..#.\n' +
                    '..#.##.....\n' +
                    '.#.#.#....#\n' +
                    '.#........#\n' +
                    '#.##...#...\n' +
                    '#...##....#\n' +
                    '.#..#...#.#\n'
                )
            );
        }
    }
}
