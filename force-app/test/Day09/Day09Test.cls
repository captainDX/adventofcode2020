@isTest
public with sharing class Day09Test {
  @isTest
  public static void ItShouldReturnFirstInvalid127() {
    System.assertEquals(
      127L,
      new Day09(new StaticResourceRepositoryMock()).part1(5)
    );
  }

  @isTest
  public static void ItShouldReturnEncryptionWeakness62() {
    System.assertEquals(
      62L,
      new Day09(new StaticResourceRepositoryMock()).part2(5)
    );
  }

  public class StaticResourceRepositoryMock implements IStaticResourceRepository {
    public StaticResource getStaticResourceByName(String name) {
      return new StaticResource(
        Body = Blob.valueOf(
          '35\n' +
          '20\n' +
          '15\n' +
          '25\n' +
          '47\n' +
          '40\n' +
          '62\n' +
          '55\n' +
          '65\n' +
          '95\n' +
          '102\n' +
          '117\n' +
          '150\n' +
          '182\n' +
          '127\n' +
          '219\n' +
          '299\n' +
          '277\n' +
          '309\n' +
          '576\n'
        )
      );
    }
  }
}
