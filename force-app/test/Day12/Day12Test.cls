@isTest
public with sharing class Day12Test {
  @isTest
  public static void itShouldReturnManhattanDistance25() {
    System.assertEquals(
      25,
      new Day12(new StaticResourceRepositoryMock()).part1()
    );
  }

  @isTest
  public static void itShouldReturnManhattanDistance286() {
    System.assertEquals(
      286,
      new Day12(new StaticResourceRepositoryMock()).part2()
    );
  }

  public class StaticResourceRepositoryMock implements IStaticResourceRepository {
    public StaticResource getStaticResourceByName(String name) {
      return new StaticResource(
        Body = Blob.valueOf('F10\n' + 'N3\n' + 'F7\n' + 'R90\n' + 'F11\n')
      );
    }
  }
}
