@isTest
public with sharing class Day02Test {
  @isTest
  public static void itShouldReturnThreeValidPasswords() {
    System.assertEquals(
      3,
      new Day02(new StaticResourceRepositoryMock()).part1()
    );
  }

  @isTest
  public static void itShouldReturnTwoValidPasswords() {
    System.assertEquals(
      2,
      new Day02(new StaticResourceRepositoryMock()).part2()
    );
  }

  public class StaticResourceRepositoryMock implements IStaticResourceRepository {
    public StaticResource getStaticResourceByName(String name) {
      return new StaticResource(
        Body = Blob.valueOf(
          '1-3 a: abcde\n' +
          '1-3 b: cdefg\n' +
          '2-9 c: ccccccccc\n' +
          '1-10 c: cccccccccd'
        )
      );
    }
  }
}
