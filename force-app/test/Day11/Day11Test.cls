@isTest
public with sharing class Day11Test {
  @isTest
  public static void ItShouldReturn37OccupiedSeats() {
    Test.startTest();
    new Day11(new StaticResourceRepositoryMock()).part1();
    Test.stopTest();
    System.assertEquals(
      37,
      [SELECT Number__c FROM Result__c WHERE Name = 'part1']
      .Number__c
    );
  }

  @isTest
  public static void ItShouldReturn37OccupiedSeatsBatch() {
    Test.startTest();
    Database.executeBatch(
      new Day11Batch(new StaticResourceRepositoryMock(), 'part1')
    );
    Test.stopTest();
    System.assertEquals(
      37,
      [SELECT Number__c FROM Result__c WHERE Day__c = 11 AND Name = 'part1']
      .Number__c
    );
  }

  @isTest
  public static void ItShouldReturn26OccupiedSeats() {
    Test.startTest();
    new Day11(new StaticResourceRepositoryMock()).part2();
    Test.stopTest();
    System.assertEquals(
      26,
      [SELECT Number__c FROM Result__c WHERE Name = 'part2']
      .Number__c
    );
  }

  @isTest
  public static void ItShouldReturn26OccupiedSeatsBatch() {
    Test.startTest();
    Database.executeBatch(
      new Day11Batch(new StaticResourceRepositoryMock(), 'part2')
    );
    Test.stopTest();
    System.assertEquals(
      26,
      [SELECT Number__c FROM Result__c WHERE Day__c = 11 AND Name = 'part2']
      .Number__c
    );
  }

  public class StaticResourceRepositoryMock implements IStaticResourceRepository {
    public StaticResource getStaticResourceByName(String name) {
      return new StaticResource(
        Body = Blob.valueOf(
          'L.LL.LL.LL\n' +
          'LLLLLLL.LL\n' +
          'L.L.L..L..\n' +
          'LLLL.LL.LL\n' +
          'L.LL.LL.LL\n' +
          'L.LLLLL.LL\n' +
          '..L.L.....\n' +
          'LLLLLLLLLL\n' +
          'L.LLLLLL.L\n' +
          'L.LLLLL.LL\n'
        )
      );
    }
  }
}
